*** Settings ***
Resource          ../Common/CommonLibrary.robot

*** Keywords ***
CLEAR_PAGE_ELEMENT
    Click Element    xpath=(//button)[8]

Document Search "Document No" by Specific All of Cashier Code Should Success
    Click Element    ${CASHIERL_CHECKBOX}
    Click Element    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search-result > div.search-result-row > div > div > div.pagination-bottom > div > div.-center > span.select-wrap.-pageSizeOptions > select
    Select From List By Value    //select    100
    Click Element    ${SEARCH_BUTTON}
    Page Should Contain    ${DOCUMENT_NO_VALUES}

Document Search "Document No" by Specific Code of Company
    Click Element    ${COMPANY_CHECKBOX}
    Input Text    ${COMPANY_INPUT}    ${COMPANY_VALUE}
    Click Element    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search-result > div.search-result-row > div > div > div.pagination-bottom > div > div.-center > span.select-wrap.-pageSizeOptions > select
    Select From List By Value    //select    100
    Click Element    ${SEARCH_BUTTON}
    Page Should Contain    ${DOCUMENT_NO_VALUES}

Document Search "Document No" by Specific Date Should Success
    Click Element    ${STARTDATE_INPUT}
    Click Element    ${STARTDATE_VALUE}
    Click Element    ${ENDDATE_INPUT}
    Click Element    ${ENDATE_VALUE}
    Click Element    ${SEARCH_BUTTON}
    Page Should Contain    TUC    #เจอใบไหนก็ได้ในช่วงวันที่ 2

Document Search "Document No" by Specific Document Type Should Success
    Click Element    ${DOCTYPE_BUTTON}
    Click Element    xpath=(//div[@class="rt-td"])[73]
    Wait Until Element Is Visible    xpath=(//button)[5]    5 Seconds
    sleep    1 Seconds
    Click Element    ${SEARCH_BUTTON}
    #${List_Docno}=    Get Text    //*[@id="app"]/div/div[1]/div[2]/div[3]/div[3]/div[1]/div/div/div[1]/div[2]/div[1]/div/div[2]
    #LOG    ${List_Docno}=
    sleep    1 Seconds
    Page Should Contain    RMV

Document Search "Document No" by Specific No of Document Should Success
    SLEEP    1 Seconds
    Click Element    ${DOCUMENTNO_INPUT}
    Input Text    ${DOCUMENTNO_INPUT}    ${DOCUMENT_NO_VALUES}
    Click Element    ${SEARCH_BUTTON}
    Page Should Contain    ${DOCUMENT_NO_VALUES}

Document Search "Document No" by Specific No of Terminal ID Should Success
    Click Element    ${TERMINAL_CHECKBOX}
    Input Text    ${TERMINAL_INPUT}    ${TERMINAL_NO}
    Click Element    ${SEARCH_BUTTON}
    Click Element    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search-result > div.search-result-row > div > div > div.pagination-bottom > div > div.-center > span.select-wrap.-pageSizeOptions > select
    Select From List By Value    //select    100
    Page Should Contain    ${DOCUMENT_NO_VALUES}

Search Reprint Suit Setup
    Create HTTP Context    @{HOST}
    LOG    ${DATA_SENDER}
    ${tempsys}    Get Current Date
    ${DATA_SENDER}    Replace String    ${DATA_SENDER}    2018-05-07T13:15:37.076+0700    ${tempsys}+0700
    ${DATA_SENDER}    Replace String    ${DATA_SENDER}    ${SPACE}    T
    LOG    ${DATA_SENDER}
    Set Request Body    ${DATA_SENDER}
    Set Request Header    @{DEFAULT_HEADER}
    Set Request Header    @{DEFAULT_HEADER2}
    HttpLibrary.HTTP.POST    ${HEEP_LIBRARY}
    Response Body Should Contain    TRM-PM-100
    ${LOGRES}    Get Response Body
    Connect To Database Using Custom Params    cx_Oracle    ${DB_CONNECT_STRING}
    ${queryResults}    Query    ${QUERY_VERIFY_EXISTDATABASE}
    LOG    ${queryResults}
    @{tetes}    Convert To List    ${queryResults}
    Log List    ${tetes}
    ${la}    Get From List    ${tetes}    0
    log    ${la}
    Convert To String    ${la}
    LOG    ${la[0]}
    ${DOCUMENT_NO_VALUES}    Get Variable Value    ${la[0]}
    LOG    ${DOCUMENT_NO_VALUES}
    Set Global Variable    ${DOCUMENT_NO_VALUES}
    Open Browser    http://172.16.3.60:18766/?shop_code=80000124#/reprint    Chrome
    Maximize Browser Window
    Set Selenium Speed    1
