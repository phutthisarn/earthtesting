*** Settings ***
Suite Setup       Keyword_Search_Reprint.Search Reprint Suit Setup
Suite Teardown    Close All Browsers
Resource          ../Common/CommonKeywords.robot
Resource          ../Common/CommonLibrary.robot

*** Variables ***
#Component on Reprint Pages
${DOCTYPE_BUTTON}    //button
${SEARCH_BUTTON}    css=div.search-col > button[color="info"]
${REPRINT_HOMEPAGE}    http://172.16.3.60:18766/?shop_code=80000124&login_name=mnpay01&employee_id=ctest_01#/reprint
${DOCUMENT_NO_VALUEOLD}    fXA100328
${DOCUMENTNO_INPUT}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(1) > div:nth-child(4) > div > input[type="text"]
${TERMINAL_CHECKBOX}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(2) > div.search-col.sm > input[type="checkbox"]
${TERMINAL_INPUT}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(2) > div:nth-child(2) > div > input[type="text"]
${TERMINAL_NO}    PB20000000100
${STARTDATE_INPUT}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(2) > div:nth-child(4) > div > div > div.react-datepicker__input-container > input
${STARTDATE_VALUE}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(2) > div:nth-child(4) > div > div > div.react-datepicker__portal > div > div.react-datepicker__month-container > div.react-datepicker__month > div:nth-child(1) > div.react-datepicker__day.react-datepicker__day--wed    #2 of may 2018
${ENDDATE_INPUT}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(2) > div:nth-child(5) > div > div > div.react-datepicker__input-container > input
${ENDATE_VALUE}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(2) > div:nth-child(5) > div > div > div.react-datepicker__portal > div > div.react-datepicker__month-container > div.react-datepicker__month > div:nth-child(1) > div.react-datepicker__day.react-datepicker__day--wed    # 2 of May 2018
${COMPANY_CHECKBOX}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(3) > div.search-col.sm > input[type="checkbox"]
${COMPANY_INPUT}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(3) > div:nth-child(2) > div > input[type="text"]
${COMPANY_VALUE}    RMV
${CASHIERL_CHECKBOX}    css=#app > div > div.reprint > div:nth-child(3) > div:nth-child(3) > div.search > div.search-content > div:nth-child(3) > div:nth-child(5) > input[type="checkbox"]
@{HOST}           172.19.192.116:8980    http
@{DEFAULT_HEADER}    Content-Type    application/json
@{DEFAULT_HEADER2}    Accept    application/json
${HEEP_LIBRARY}    /pss-trm-web/rest/paymentservice/savepayment
${DATA_SENDER}    {"cashier-id":"ctest_01","cashier-name":"ctest1","cust-list":[{"customer-info":{"tax-id":"9999999","title-name":"Mr.","first-name":"Muk","last-name":"Mukdathong","branch-info":"TEST1","certificate-id":"0105554072452","printcustomerinfo":"","change-addr-ind":"N","customer-type":"Corporate","address-no":"700/616","address":"buildingAMATANAKORNINDUSTRIALESTATE","moo":"4","road":"Bankao","soi":"","trog":"","tumbol":"napradoo","amphur":"panthong","province":"chonburi","postcode":"20160"},"fullform-flag":"0","item-list":[{"hp-qr-trxid":null,"hp-qr-channel":null,"order-id":null,"temp-interface-order-jsonstr":null,"acct-num":null,"cust-serv-num":null,"api-system":"MCP","billing-system":"CCBS","service-code":"SVRMV001","operator-company-branch-name":"Headoffice","operator-company-branch-no":".","operator-company-id":"RMV","operator-company-tax-id":"0105549112786","represent-company-id":"TPay","invoice-no":"011120171000071671","invoice-type":"BILL","start-date":"2017-11-01T00:00:00.000+0700","end-date":"2017-11-30T00:00:00.000+0700","start-service-date":"2017-10-25T17:54:11.000+0700","reference1":"200053830","reference2":"0863200441","ban-status":"Opened","manual-product":"0","manual-overpayment":"0","barcode":"","convergence-code":"","round-updown":"0.00","vat-rate":"7.00","vat-code":"VAT","vat-amount":"28.87","total-amount":"441.26","bill-amount":441.26,"discount-amount":null,"paid-amount":"441.26","bill-amount-left":441.26,"receive-amount":"441.26","change-amount":"0.00","cash-net":"441.26","amount-before-vat":"412.39","document-type":"02","cslockmechanism":null,"mode":"ONLINE","creditcard-info":null,"cheque-info":null,"wht-info":{"paid-amount":"","wht-amount":"","wht-date":"2018-05-07T13:15:37.076+0700","wht-docno":"","wht-ind":"N","wht-rate":"","wht-original":"N"},"other-info":null}],"referenced-doc-no":""}],"document-date":"2018-05-07T13:15:37.076+0700","post-date":"2018-05-07T13:15:37.076+0700","shop-id":"TEST1","terminal-cpuid":"61641638598d0c59","terminal-id":"FW_JUB","from-system":"redesign"}
${DB_CONNECT_STRING}    'trmapp201805/freewill@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.19.192.114)(PORT=1553))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=TRMUAT)))'
${QUERY_VERIFY_EXISTDATABASE}    select * from(select documentno from document order by CREATEDATE DESC) where rownum = 1

*** Test Cases ***
Document Search "Document No" by Specific document type [tc_TC_ENQ_RP_001]
    [Documentation]    การค้นหาพิมพ์ซ้ำเอกสารด้วย "ประเภทเอกสาร" โดยระบุประเภทเอกสาร
    [Tags]    Positive Test Case
    Document Search "Document No" by Specific Document Type Should Success
    CLEAR_PAGE_ELEMENT

Document Search "Document No" by Specific No of Document [tc_TC_ENQ_RP_003]
    [Documentation]    การค้นหาพิมพ์ซ้ำเอกสารด้วย "เลขที่เอกสาร"
    [Tags]    Positive Test Case
    Document Search "Document No" by Specific No of Document Should Success
    CLEAR_PAGE_ELEMENT

Document Search "Document No" by Specific No of Terminal [tc_TC_ENQ_RP_004]
    [Documentation]    การค้นหาพิมพ์ซ้ำเอกสารด้วย "เลขที่เครื่องเทอมินัล (Cash Register)"
    [Tags]    Positive Test Case
    Document Search "Document No" by Specific No of Terminal ID Should Success
    CLEAR_PAGE_ELEMENT

Document Search "Document No" by specific Datetime [tc_TC_ENQ_RP_005]
    [Documentation]    การค้นหาพิมพ์ซ้ำเอกสารด้วย "วันที่เอกสาร" โดยระบุวันที่เอกสาร
    Document Search "Document No" by Specific Date Should Success
    CLEAR_PAGE_ELEMENT

Document Search "Document No" by Specific Code of Company [tc_TC_ENQ_RP_006]
    [Documentation]    การค้นหาพิมพ์ซ้ำเอกสารด้วย "บริษัทตัวการ" \ โดยระบุบริษัทตัวการ
    Document Search "Document No" by Specific Code of Company
    CLEAR_PAGE_ELEMENT

Document Search "Document No" by Specific all of cashier code [tc_TC_ENQ_009]
    [Documentation]    การค้นหาพิมพ์ซ้ำเอกสารด้วย "รหัสพนักงาน" ทุกรหัสพนักงาน
    Document Search "Document No" by Specific All of Cashier Code Should Success
    CLEAR_PAGE_ELEMENT
