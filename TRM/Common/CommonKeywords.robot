*** Settings ***
Resource          ../Keywords/Keywords_Search_Cancel_Payment.robot    #####Cancel Payment#####
Resource          ../Keywords/Keywords_Cancel_Payment.robot
Resource          ../Keywords/Keywords_Search_Change_Address.robot    #####Change Address#####
Resource          ../Keywords/Keywords_Change Address.robot
Resource          ../Keywords/Keyword_Search_Reprint.robot
Resource          ../Keywords/Keyword_Save_Reprint.robot
Resource          ../Keywords/Keyword_Search_Abb.robot

*** Keywords ***
Create Document Current Date
    ######CREATE DOCUMENT######
    Get Current Date 01
    Create Document
    Get DocNo From Database

Open Cancel Payment Page
    Open Browser    @{URL_CANCEL_PAYMENT}
    Maximize Browser Window
    Set Selenium Speed    1s

Open Change Address Page
    Open Browser    @{URL_CHANGE_ADDRESS}
    Maximize Browser Window
    Set Selenium Speed    1s

Create Document
    Create HTTP Context    @{HOST}
    Set Request Body    ${Body_Savepayment_Sysdate}
    Set Request Header    @{Content-Type_Json}
    Set Request Header    @{Accept-Type_Json}
    HttpLibrary.HTTP.POST    ${HTTP.POST_SAVEPAYMENT}
    Log Response Body

Get DocNo From Database
    Connect to Database using Custom Params    cx_Oracle    '${DBUSER_TRMUAT}/${DBPWD_TRMUAT}@${DBHOST}:${DBPORT}/${DBNAME_TRMUAT}'
    ${Doc_From_Database_Sysdate}=    Query    SELECT documentno FROM (SELECT * FROM document ORDER BY CREATEDATE DESC) WHERE ROWNUM = 1
    Log    ${Doc_From_Database_Sysdate[0]}
    Set Global Variable    ${Doc_From_Database_Sysdate[0]}

Get Current Date 01
    ${date}=    DateTime.Get Current Date
    Log    ${date}
    @{split_date}=    Split String    ${date}    ${SPACE}
    ${Body_Savepayment_Sysdate}=    Replace String    ${Body_Savepayment}    2018-01-01    @{split_date}[0]
    Log    ${Body_Savepayment}
    Log    ${Body_Savepayment_Sysdate}
    Set Global Variable    ${Body_Savepayment_Sysdate}
